package com.fueled.sample.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fueled.sample.R;
import com.fueled.sample.constants.AppConstants;
import com.fueled.sample.database.DBOperation;
import com.fueled.sample.entities.RestaurantEntity;
import com.fueled.sample.interfaces.DialogClickInterface;
import com.fueled.sample.response.VenueApiResponse;
import com.fueled.sample.retrofit.ApiEndpoints;
import com.fueled.sample.retrofit.ApiService;
import com.fueled.sample.util.CommonUtil;
import com.fueled.sample.util.CustomPopup;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener;
import com.google.android.gms.maps.GoogleMap.OnCameraMoveListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, AppConstants,
        ConnectionCallbacks, LocationListener, OnConnectionFailedListener, OnClickListener,
        OnMarkerClickListener, OnCameraMoveListener, OnCameraIdleListener {

    private GoogleMap googleMap;
    private List<VenueApiResponse.Venue> venues;
    private List<Marker> markers;
    private Marker marker, myMarker;
    private VenueApiResponse.Venue venue;
    private ApiEndpoints apiEndpoints;
    private CustomPopup customPopup;
    private LocationManager locationManager;
    private LocationRequest locationRequest;
    private Handler handler;
    private Toolbar toolbar;
    private Call<VenueApiResponse> call;
    private boolean controlFlag, apiFlag;
    private GoogleApiClient googleApiClient;
    private Double newLat, newLng, myLat, myLng;
    private Calendar calendar;
    private DecimalFormat format = new DecimalFormat("##.#####");
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(REQUEST_DATE_FORMAT);
    private DBOperation dbOperation;

    private BottomSheetBehavior bottomSheetBehavior;
    private TextView tvRestName, tvRestAdd, tvUser, tvCheckIn;
    private ProgressBar progressBar;
    private ImageView img_thumb_up, img_thumb_down;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apiEndpoints = ApiService.getApiEndpoints();
        customPopup = new CustomPopup();
        handler = new Handler();
        markers = new ArrayList<>();
        calendar = Calendar.getInstance();
        controlFlag = true;
        apiFlag = true;
        dbOperation = new DBOperation(getApplicationContext());

        /*
        Initialising UI components
         */
        progressBar = (ProgressBar) findViewById(R.id.progress);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_home));
        tvRestName = (TextView) findViewById(R.id.tv_restaurant_name);
        tvRestAdd = (TextView) findViewById(R.id.tv_restaurant_add);
        tvUser = (TextView) findViewById(R.id.tv_user_count);
        tvCheckIn = (TextView) findViewById(R.id.tv_check_count);
        img_thumb_up = (ImageView) findViewById(R.id.img_thumb_up);
        img_thumb_down = (ImageView) findViewById(R.id.img_thumb_down);
        View bottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CONSTANT) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        getMap();
                    }
                }
            }
        }
    }

    void getMap() {
        if (googleMap == null) {
            SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            supportMapFragment.getMapAsync(this);
        }
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            createLocationRequest();
        }
    }

    protected void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(RETRY_TIME);
        locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonUtil.requestLocationPermission(this))
            getMap();
        checkLocation();
        if (markers.size() == 0 && myLat != null && myLng != null) {
            getRestaurants(new LatLng(myLat, myLng));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnCameraMoveListener(this);
        googleMap.setOnCameraIdleListener(this);
        this.googleMap = googleMap;
    }

    /*
    Function to check whether location services are enabled or not
     */
    void checkLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (!gps_enabled && !network_enabled) {
            customPopup.createPopup(getString(R.string.gps_network_not_enabled), getString(R.string.app_setting), getString(R.string.app_exit), this, new DialogClickInterface() {
                @Override
                public void positiveClick(DialogInterface dialog) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                }

                @Override
                public void negativeClick(DialogInterface dialog) {
                    finish();
                }

            });
        }
    }

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }

    /*
    Called when Google Api Client is connected
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        //Initialising fused location API
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        newLat = Double.parseDouble(format.format(location.getLatitude()));
        newLng = Double.parseDouble(format.format(location.getLongitude()));
        Log.e("Location Update : ", newLat + " " + newLng);
        if ((myLat == null && myLng == null)
                || (!newLat.equals(myLat) && !newLng.equals(myLng))) {
            //Update only when there is a change in location
            myLat = newLat;
            myLng = newLng;
            LatLng myLoc = new LatLng(myLat, myLng);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(myLoc);
            if (myMarker != null) {
                myMarker.setPosition(myLoc);
            } else {
                myMarker = googleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.icon_my_loc))
                        .position(myLoc)
                        .title("My Location"));
            }
            if (controlFlag) {
                googleMap.animateCamera(cameraUpdate);
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_current_loc:
                controlFlag = true; //to allow LatLong Bounds
                hideBottomSheet();
                //move camera to current location
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(new LatLng(myLat, myLng));
                googleMap.animateCamera(cameraUpdate);
                break;
            case R.id.container:
                hideBottomSheet();
                break;

            case R.id.img_thumb_up:
                if(venue!=null) {
                    //Update Like in DB
                    new UpdatePlaceInDB().execute(venue, 1);
                }
                break;

            case R.id.img_thumb_down:
                if(venue!=null) {
                    //Update Dislike in DB
                    new UpdatePlaceInDB().execute(venue, -1);
                }
                break;
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        try {
            controlFlag = false; // disable LatLong bounds
            apiFlag = false;    // disable api call
            this.marker = marker;
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .zoom(googleMap.getCameraPosition().zoom)
                    .tilt(googleMap.getCameraPosition().tilt)
                    .target(marker.getPosition())
                    .build();

            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            googleMap.animateCamera(cameraUpdate);
            int index = markers.indexOf(marker);
            venue = venues.get(index);
            tvRestName.setText(venue.getName());
            StringBuilder addBuilder = new StringBuilder();
            for (String s : venue.getLocation().getFormattedAddress()) {
                addBuilder.append(s).append(" ");
            }
            tvRestAdd.setText(addBuilder.toString());
            tvCheckIn.setText(venue.getStats().getCheckinsCount() == null ? "0" : venue.getStats().getCheckinsCount().toString());
            tvUser.setText(venue.getStats().getUsersCount() == null ? "0" : venue.getStats().getUsersCount().toString());

            new GetPlaceFromDB().execute(venue.getId()); // get Like/Dislike from DB

        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    void showBottomSheet() {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    void hideBottomSheet() {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
        try {
            dbOperation.getSqLiteDatabase().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCameraMove() {
        handler.removeCallbacksAndMessages(null);
        if (call != null) {
            call.cancel();
        }
        // added delay to api call map position change
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                LatLng latLng = googleMap.getCameraPosition().target;
                if (apiFlag) {
                    getRestaurants(latLng);
                }
            }
        }, MAP_WAIT_TIME);
    }

    void loadMarkers() {
        for (Marker marker : markers)
            marker.remove();
        markers.clear();
        LatLngBounds.Builder builder = null;

        if (controlFlag) {
            builder = new LatLngBounds.Builder();
        }
        for (VenueApiResponse.Venue venue : venues) {
            LatLng latLng = new LatLng(venue.getLocation().getLat(), venue.getLocation().getLng());
            MarkerOptions marker = new MarkerOptions().
                    position(latLng).
                    title(venue.getName()).
                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).
                    draggable(false);
            markers.add(googleMap.addMarker(marker));
            if (controlFlag) {
                builder.include(latLng);
            }
        }
        if (controlFlag) {
            controlFlag = false;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(builder.build(), 10);
            googleMap.animateCamera(cu);
        }
    }

    void getRestaurants(LatLng latLng) {
        progressBar.setVisibility(View.VISIBLE);
        call = apiEndpoints.getRestaurants(simpleDateFormat.format(calendar.getTime()), SEARCH_RADIUS,
                latLng.latitude + "," + latLng.longitude, "restaurant");
        call.enqueue(new Callback<VenueApiResponse>() {
            @Override
            public void onResponse(Call<VenueApiResponse> call, Response<VenueApiResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.errorBody() != null) {
                    String error = response.code() + " : " + response.message();
                    Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT).show();
                } else {
                    // Adding search results in Database
                    venues = response.body().getResponse().getVenues();
                    new AddPlacesInDB().execute(venues);
                }
            }

            @Override
            public void onFailure(Call<VenueApiResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                if (t.toString().toLowerCase().contains("sockettimeout")) {
                    Toast.makeText(MainActivity.this, getString(R.string.error_timeout), Toast.LENGTH_SHORT).show();
                } else if (!t.toString().toLowerCase().contains("cancel")) {
                    Toast.makeText(MainActivity.this, getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onCameraIdle() {
        if (marker != null) {
            marker.showInfoWindow();
            showBottomSheet();
            marker = null;
            //allow api call
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    apiFlag = true;
                }
            }, MAP_WAIT_TIME + 10);
        }
    }

    class AddPlacesInDB extends AsyncTask<List<VenueApiResponse.Venue>, Void, List<VenueApiResponse.Venue>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<VenueApiResponse.Venue> doInBackground(List<VenueApiResponse.Venue>... lists) {
            List<VenueApiResponse.Venue> venues = lists[0];
            for (VenueApiResponse.Venue venue : venues) {
                dbOperation.putInfo(dbOperation, venue, 0, false); // keeping the update flag false
            }
            return venues;
        }

        @Override
        protected void onPostExecute(List<VenueApiResponse.Venue> venues) {
            MainActivity.this.venues = venues;
            loadMarkers();
            progressBar.setVisibility(View.GONE);
        }
    }

    class GetPlaceFromDB extends AsyncTask<String, Void, RestaurantEntity> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected RestaurantEntity doInBackground(String... strings) {
            return dbOperation.getInfo(dbOperation, strings[0]);
        }

        @Override
        protected void onPostExecute(RestaurantEntity restaurantEntity) {
            progressBar.setVisibility(View.GONE);
            // setting the like/dislike value obtained from DB
            switch (restaurantEntity.getPollValue()) {
                case -1:
                    img_thumb_down.setImageResource(R.drawable.icon_thumb_down_fill);
                    img_thumb_up.setImageResource(R.drawable.icon_thumb_up);
                    break;

                case 0:
                    img_thumb_down.setImageResource(R.drawable.icon_thumb_down);
                    img_thumb_up.setImageResource(R.drawable.icon_thumb_up);
                    break;

                case 1:
                    img_thumb_down.setImageResource(R.drawable.icon_thumb_down);
                    img_thumb_up.setImageResource(R.drawable.icon_thumb_up_fill);
                    break;
            }
        }
    }

    class UpdatePlaceInDB extends AsyncTask<Object, Void, Integer> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Integer doInBackground(Object... objects) {
            dbOperation.putInfo(dbOperation, (VenueApiResponse.Venue) objects[0], (Integer) objects[1], true);
            return (Integer) objects[1];
        }

        @Override
        protected void onPostExecute(Integer integer) {
            progressBar.setVisibility(View.GONE);
            // setting the like/dislike value in DB
            switch (integer.intValue()) {
                case -1:
                    img_thumb_down.setImageResource(R.drawable.icon_thumb_down_fill);
                    img_thumb_up.setImageResource(R.drawable.icon_thumb_up);
                    break;

                case 0:
                    img_thumb_down.setImageResource(R.drawable.icon_thumb_down);
                    img_thumb_up.setImageResource(R.drawable.icon_thumb_up);
                    break;

                case 1:
                    img_thumb_down.setImageResource(R.drawable.icon_thumb_down);
                    img_thumb_up.setImageResource(R.drawable.icon_thumb_up_fill);
                    break;
            }
        }
    }
}

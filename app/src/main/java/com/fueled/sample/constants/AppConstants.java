package com.fueled.sample.constants;

/**
 * Created by Saurabh on 9/17/16.
 */
public interface AppConstants {
    String REQUEST_DATE_FORMAT = "yyyyMMdd"; // for date format in api CALL

    int PERMISSION_CONSTANT= 6;
    long RETRY_TIME = 10000;    // LOCATIOn retry time
    long SEARCH_RADIUS = 5000;
    long MAP_WAIT_TIME = 500;
}

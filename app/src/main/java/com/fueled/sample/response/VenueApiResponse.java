package com.fueled.sample.response;

import java.util.List;

/**
 * Created by Saurabh on 9/17/16.
 */
public class VenueApiResponse {
    // POJO class for API response
    private Data response;

    public Data getResponse() {
        return response;
    }

    public class Data {
        private List<Venue> venues;

        public List<Venue> getVenues() {
            return venues;
        }
    }

    public class Venue {
        private String id;
        private String name;
        private List<Category> categories;
        private Location location;
        private Stats stats;

        public Stats getStats() {
            return stats;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public List<Category> getCategories() {
            return categories;
        }

        public Location getLocation() {
            return location;
        }

        public class Location {
            private Double lat;
            private Double lng;
            private Long distance;
            private List<String> formattedAddress;

            public Double getLat() {
                return lat;
            }

            public Double getLng() {
                return lng;
            }

            public Long getDistance() {
                return distance;
            }

            public List<String> getFormattedAddress() {
                return formattedAddress;
            }
        }

        public class Category {
            private String name;

            public String getName() {
                return name;
            }
        }

        public class Stats{
            private Integer checkinsCount;
            private Integer usersCount;

            public Integer getCheckinsCount() {
                return checkinsCount;
            }

            public Integer getUsersCount() {
                return usersCount;
            }
        }


    }

}

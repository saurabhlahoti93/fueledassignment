package com.fueled.sample.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.fueled.sample.entities.RestaurantEntity;
import com.fueled.sample.response.VenueApiResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cl-macmini-80 on 9/17/16.
 */
public class DBOperation extends SQLiteOpenHelper {
    private final static int DATABASE_VERSION = 1;
    private String createQuery = "create table " + TableInfo.TABLE_NAME + " (" + TableInfo.PLACE_ID + " text, " +
            TableInfo.PLACE_NAME + " text, " + TableInfo.POLL_VALUE + " integer); ";
    private SQLiteDatabase sqLiteDatabase;
    public DBOperation(Context context) {
        super(context, TableInfo.DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createQuery);
        Log.i("database", "successfully table created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }

    public SQLiteDatabase getSqLiteDatabase() {
        return sqLiteDatabase;
    }

    public void putInfo(DBOperation dbOperation, VenueApiResponse.Venue venue, int poll, boolean isUpdate) {
         sqLiteDatabase = dbOperation.getWritableDatabase();
        if (getInfo(dbOperation, venue.getId()) == null) {    // add value in DB only if it not present
            ContentValues values = new ContentValues();
            values.put(TableInfo.PLACE_ID, venue.getId());
            values.put(TableInfo.PLACE_NAME, venue.getName());
            values.put(TableInfo.POLL_VALUE, poll);
            sqLiteDatabase.insert(TableInfo.TABLE_NAME, null, values);
        } else {
            if(isUpdate) {
                String where = TableInfo.PLACE_ID + " = ?";
                String[] whereArgs = {venue.getId()};
                ContentValues values = new ContentValues();
                values.put(TableInfo.PLACE_ID, venue.getId());
                values.put(TableInfo.PLACE_NAME, venue.getName());
                values.put(TableInfo.POLL_VALUE, poll);
                sqLiteDatabase.update(TableInfo.TABLE_NAME, values, where, whereArgs);
            }
        }
    }

    public RestaurantEntity getInfo(DBOperation dbOperation, String id) {
        sqLiteDatabase = dbOperation.getReadableDatabase();
        String[] tableColumns = {TableInfo.PLACE_ID, TableInfo.PLACE_NAME, TableInfo.POLL_VALUE};
        String[] whereArgs = {id};
        Cursor cursor = sqLiteDatabase.query(TableInfo.TABLE_NAME, tableColumns, TableInfo.PLACE_ID + " = ?", whereArgs, null, null, null);
        RestaurantEntity restaurantEntity = null;
        cursor.moveToFirst();
        do {
            try {
                restaurantEntity = new RestaurantEntity(cursor.getString(0), cursor.getString(1), cursor.getInt(2));
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        } while (cursor.moveToNext());
        return restaurantEntity;
    }


    public List<RestaurantEntity> getTableData(DBOperation dbOperation) {
        sqLiteDatabase = dbOperation.getReadableDatabase();
        String[] tableColumns = {TableInfo.PLACE_ID, TableInfo.PLACE_NAME, TableInfo.POLL_VALUE};
        List<RestaurantEntity> restaurantEntities = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.query(TableInfo.TABLE_NAME, tableColumns, null, null, null, null, null);
        RestaurantEntity restaurantEntity = null;
        cursor.moveToFirst();
        do {
            try {
                restaurantEntity = new RestaurantEntity(cursor.getString(0), cursor.getString(1), cursor.getInt(2));
                restaurantEntities.add(restaurantEntity);
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        } while (cursor.moveToNext());
        return restaurantEntities;
    }
}


package com.fueled.sample.database;

import android.provider.BaseColumns;

/**
 * Created by cl-macmini-80 on 9/17/16.
 */
public abstract class TableInfo implements BaseColumns{
    // table Columns
    public static final String PLACE_ID = "place_id";
    public static final String PLACE_NAME = "place_name";
    public static final String POLL_VALUE = "poll_value";
    public static final String DATABASE_NAME = "fueled_db";
    public static final String TABLE_NAME = "place_info";
}

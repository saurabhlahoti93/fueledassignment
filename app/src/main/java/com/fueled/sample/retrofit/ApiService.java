package com.fueled.sample.retrofit;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Saurabh on 9/17/16.
 */
public class ApiService {
    private static ApiEndpoints apiEndpoints;
    final static String BASE_URL = "https://api.foursquare.com/v2/";
    final static String CLIENT_SECRET = "FO1YWOMEZUT2DA2A1P3WUOCQYSLVUAS3KQAOGYGTKQDDI1LJ";
    final static String CLIENT_ID = "5NQQPSYPQN3FCMNH5VBBK12JYZMPVXZKHTNG4I4JPDVHNCO1";

    public static ApiEndpoints getApiEndpoints() {
        if (apiEndpoints == null) {
            OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            // adding interceptor to put common query params
            okHttpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    HttpUrl originalUrl = original.url();
                    HttpUrl modifiedUrl = originalUrl.newBuilder()
                            .addQueryParameter("client_secret",CLIENT_SECRET)
                            .addQueryParameter("client_id", CLIENT_ID)
                            .build();
                    Request.Builder requestBuilder = original.newBuilder()
                            .url(modifiedUrl);

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
            okHttpClient.interceptors().add(interceptor);
            okHttpClient.readTimeout(15, TimeUnit.SECONDS);
            okHttpClient.connectTimeout(15, TimeUnit.SECONDS);
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient.build());
            Retrofit adapter = builder.build();
            apiEndpoints = adapter.create(ApiEndpoints.class);
        }
        return apiEndpoints;
    }

}

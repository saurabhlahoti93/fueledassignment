package com.fueled.sample.retrofit;

import com.fueled.sample.response.VenueApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Saurabh on 9/17/16.
 */
public interface ApiEndpoints {

    @GET("venues/search")
    Call<VenueApiResponse> getRestaurants(@Query("v") String dateVersion,
                                          @Query("radius") long radius,
                                          @Query("ll") String latLng,
                                          @Query("query") String searchFor);
}
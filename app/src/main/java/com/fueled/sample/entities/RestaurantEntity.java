package com.fueled.sample.entities;

/**
 * Created by cl-macmini-80 on 9/17/16.
 */
public class RestaurantEntity {
    private String placeName;
    private String placeId;
    private int pollValue;

    public RestaurantEntity(String placeId, String placeName, Integer pollValue) {
        this.placeName = placeName;
        this.placeId = placeId;
        this.pollValue = pollValue;
    }

    public int getPollValue() {
        return pollValue;
    }
}

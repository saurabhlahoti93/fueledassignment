package com.fueled.sample.interfaces;

import android.content.DialogInterface;

/**
 * Created by Saurabh on 9/17/16.
 */
public interface DialogClickInterface {
    void positiveClick(DialogInterface dialog);

    void negativeClick(DialogInterface dialog);
}

package com.fueled.sample.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.fueled.sample.R;
import com.fueled.sample.constants.AppConstants;

/**
 * Created by Saurabh on 9/17/16.
 */
public class CommonUtil implements AppConstants {
    static AlertDialog alertDialog;
    public static boolean requestLocationPermission(final Context context) {

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        final String[] perms = new String[1];
        perms[0] = Manifest.permission.ACCESS_FINE_LOCATION;
        if(alertDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setMessage(context.getString(R.string.perm_location));
            builder.setCancelable(false);

            // Set the action buttons
            builder.setPositiveButton(context.getString(R.string.app_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    ActivityCompat.requestPermissions((Activity) context, perms, PERMISSION_CONSTANT);
                }
            });
            builder.setNegativeButton(context.getString(R.string.app_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                }
            });
            alertDialog = builder.create();
        }else{
            alertDialog.dismiss();
        }
        alertDialog.show();
        return false;
    }
}

package com.fueled.sample.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.fueled.sample.interfaces.DialogClickInterface;

/**
 * Created by Saurabh on 9/17/16.
 */
public class CustomPopup {
// Custom POpup Class
    public void createPopup(String message, String positiveText, String negativeText, Context context,
                            final DialogClickInterface dialogClickInterface) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(message);

        builder.setPositiveButton(positiveText
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialogClickInterface.positiveClick(dialog);
                    }
                });
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialogClickInterface.negativeClick(dialog);
                    }
                });
        builder.show();
    }
}

## Fueled - Assignment

### Android app with Local DB showing near by restaurants with option to like and dislike them for future reference

#### External dependencies and Apis used
* [Retrofit] - For making REST api calls
* OkHttp Logger - For Logging API Response
* GSON - For parsing json data to POJO
* Android Support Library
* Android Maps API
* Android Fused Location API
* FourSquare API


   [Facebook]: <https://github.com/facebook/facebook-android-sdk/>
   [Retrofit]: <https://github.com/square/retrofit/>
  
   [Picasso]: <https://github.com/square/picasso>
  